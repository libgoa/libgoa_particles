

package;

import h2d.BlendMode;
import libgoa.particles.ParticleGroup;
import libgoa.particles.ParticleSystem;
import libgoa.particles.loaders.ParticleLoader;

class Main extends hxd.App {

    var system:ParticleSystem;
    var p:h2d.Object;
    
    var groups:Array<ParticleGroup>;
    var groupIdx:Int;

    override function init() {
        p = new h2d.Object(s2d);
        p.x = 100;
        p.y = 100;
        
        groups = [];
        groups.push(ParticleLoader.load('arrow.json'));
        groups.push(ParticleLoader.load('pixi-flame.pixi'));
        groups.push(ParticleLoader.load('fire.plist'));
        groups.push(ParticleLoader.load('fountain.lap'));
        groups.push(ParticleLoader.load('campfire.pex'));
        groupIdx = 0;
        
        system = new ParticleSystem(p);
        system.addParticleGroup(groups[groupIdx]);
        system.start();
    }
    
    override function update(dt) {
        system.update(dt);
        
        if (hxd.Key.isReleased(hxd.Key.S)) {
            system.scaleGroups(.9);
        }
        
        if (hxd.Key.isReleased(hxd.Key.N)) {
            system.clear();
            groupIdx = (groupIdx + 1) % groups.length;
            system.addParticleGroup(groups[groupIdx]);
            system.start();
        }
        
        p.x = hxd.Window.getInstance().mouseX;
        p.y = hxd.Window.getInstance().mouseY;
    }

    static public function main() {
        hxd.Res.initEmbed();
        new Main();
    }
}